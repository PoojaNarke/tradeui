import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatebyComponent } from './updateby.component';

describe('UpdatebyComponent', () => {
  let component: UpdatebyComponent;
  let fixture: ComponentFixture<UpdatebyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdatebyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatebyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
