import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  tickerVal = 'NFLX';
  title = 'TradeUI';

  percent = 20;

  onPercentChange(percent: number) {
    console.log('here');  

    this.percent = percent;
  }
}

