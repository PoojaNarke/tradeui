import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { HttpClientModule } from '@angular/common/http';
import { DisplayStatusComponent } from './display-status/display-status.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GetbyidComponent } from './getbyid/getbyid.component';
import { UpdatebyidComponent } from './update/updatebyid/updatebyid.component';
import { UpdatebyComponent } from './update/updateby/updateby.component';

import { NgxEchartsModule } from 'ngx-echarts';
import { SimpleChartComponent } from './simple-chart/simple-chart.component';
import { TimeSeriesComponent } from './time-series/time-series.component';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [
    AppComponent,
    PortfolioComponent,
    DisplayStatusComponent,
    GetbyidComponent,
    UpdatebyidComponent,
    UpdatebyComponent,
    SimpleChartComponent,
    TimeSeriesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    FormsModule,
    HttpClientModule,
    DataTablesModule,
    NgbModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
