import { Component, OnInit } from '@angular/core';
import { TradeService } from '../trade.service';

@Component({
  selector: 'app-getbyid',
  templateUrl: './getbyid.component.html',
  styleUrls: ['./getbyid.component.css']
})
export class GetbyidComponent implements OnInit {
  title = 'Using Get By Id API'

  constructor(private myTradeService:TradeService) { }
  data:any
  id:number
  // button handler method
  getDataFromCode(){
    console.log('get by ID function')
    this.myTradeService.getbyid(this.id)
    .subscribe( (response)=>{this.data = response;
      //  console.log(this.data)
      } )
  }


  ngOnInit(): void {
    
  }
}
