import { Component, OnInit } from '@angular/core';
import { TradeService } from '../trade.service';

@Component({
  selector: 'app-display-status',
  templateUrl: './display-status.component.html',
  styleUrls: ['./display-status.component.css']
})
export class DisplayStatusComponent implements OnInit {

  constructor(private myTradeService:TradeService) { }

  data:any
    // button handler method
    getDataFromCode(){
      console.log('get from code')
      this.myTradeService.getall()
      .subscribe( (response)=>{this.data = response;
        //  console.log(this.data)
        } )
    }

  ngOnInit(): void {
    this.getDataFromCode()
  }

}
