import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  baseURL = 'http://citieurlinux1.conygre.com:8080/trades'
  //baseURL = 'http://localhost:8080/trades'
  insertURL = 'insert'
  getStatusURL = 'getStatus'
  getallURL = 'getall'
  getbyidURL = 'getbyid'
  constructor( private http: HttpClient ) {
      
   }

   insert(){
    let fullURL = `${this.baseURL}/${this.insertURL}`
    return this.http.get(fullURL) // this is an Observable
  }

   getStatus(){
    let fullURL = `${this.baseURL}/${this.getStatusURL}`
    return this.http.get(fullURL) // this is an Observable
  }

  getall(){
    let fullURL = `${this.baseURL}/${this.getallURL}`
    return this.http.get(fullURL) // this is an Observable
  }

  getbyid(id:number){
    return this.http.get(`${this.baseURL}/${this.getbyidURL}/${id}`);
  }

}
