import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PriceService {

  baseURL = 'https://pxqzjt6ami.execute-api.us-east-1.amazonaws.com/default/filePriceFeed'

  constructor( private http: HttpClient ) {
      
   }

   getPrices(ticker: string, num_days: number){
    let fullURL = this.baseURL + "?ticker=" + ticker + "&num_days=" + num_days
    return this.http.get(fullURL) // this is an Observable
  }

}
