import { Component, OnDestroy, OnInit, Input, ElementRef, HostListener, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { PriceService } from '../price.service';
import { HttpClient } from '@angular/common/http';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-time-series',
  templateUrl: './time-series.component.html',
  styleUrls: ['./time-series.component.css']
})


export class TimeSeriesComponent implements OnInit, OnDestroy  {

  options: any;
  updateOptions: any;

  private oneDay = 24 * 3600 * 1000;
  private now: Date;
  private value: number;
  private data: any[];
  private timer: any;
  public ticker: string;
  dtOptions: DataTables.Settings = {};
  posts;
  
  constructor(private priceService: PriceService, private http: HttpClient, private cdRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.displayChart('C');
    this.dtOptions = {
      pageLength: 5
    };
    // http://citieurlinux1.conygre.com:8080/trades/getall
    this.http.get('http://citieurlinux1.conygre.com:8080/trades/getall')
      .subscribe(posts => {
        this.posts = posts;
    });

  }

 tickerTest='C';

 displayChart1(event?: KeyboardEvent){
   this.displayChart((event.target as HTMLElement).textContent);
 }

  displayChart(tickerTest:string) {
    // generate some random testing data:
    this.data = [];
    this.now = new Date(1997, 9, 3);
    this.value = Math.random() * 1000;
    this.ticker = tickerTest;

    this.priceService.getPrices(this.ticker, 30).subscribe( (priceData) =>{
      console.log(priceData)

      /*(for (let i = 0; i < 1000; i++) {
        this.data.push(this.randomData());
      }*/
  
      for(let item in priceData['price_data']) {
        this.data.push({"name": priceData['price_data'][item][0], "value": priceData['price_data'][item]})
      }

    console.log(this.data)
    // initialize chart options:
    this.options = {
      title: {
        text: "Time Series 90 days of data for: " + this.ticker
      },
      tooltip: {
        trigger: 'axis',
        formatter: (params) => {
          params = params[0];
          const date = new Date(params.name);
          return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' : ' + params.value[1];
        },
        axisPointer: {
          animation: false
        }
      },
      xAxis: {
        type: 'time',
        splitLine: {
          show: false
        }
      },
      yAxis: {
        type: 'value',
        boundaryGap: [0, '100%'],
        splitLine: {
          show: false
        }
      },
      series: [{
        name: 'Mocking Data',
        type: 'line',
        showSymbol: false,
        hoverAnimation: false,
        data: this.data
      }]
    };

    /* Mock dynamic data:
    this.timer = setInterval(() => {
      for (let i = 0; i < 5; i++) {
        this.data.shift();
        this.data.push(this.randomData());
      }

      // update series data:
      this.updateOptions = {
        series: [{
          data: this.data
        }]
      };
    }, 1000);*/
    })
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }


  randomData() {
    this.now = new Date(this.now.getTime() + this.oneDay);
    this.value = this.value + Math.random() * 21 - 10;
    return {
      name: this.now.toString(),
      value: [
        [this.now.getFullYear(), this.now.getMonth() + 1, this.now.getDate()].join('/'),
        Math.round(this.value)
      ]
    };
  }


  
  }
